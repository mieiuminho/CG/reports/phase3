# Engine {#sec:engine}

For this application we were required to implement periodic rotation (receiving
the time that it takes the model to perform a complete turn). We were also
required to implement periodic translations (receiving the time that it takes
to perform the whole translation and the control points that define the
_Catmull-Rom_ curve).

For each model we calculate the "normalized" time (per example: if t = 12 and
the time that the model takes  to complete a full lap is 10 then the
"normalized" _t_ is 2.)

Once we have _t_ we calculate the segment of the curve (assuming that the model
takes the exact same time going through each segment).

We need to calculate the points of the _Catmull-Rom_ curve for every value of
_t_. As we went over during classes we can calculate the new point as follows:

* $P(t) =
    \begin{bmatrix}
    1 & t & t^{2} & t^{3}
    \end{bmatrix}
    \begin{bmatrix}
    -0.5 & 1.5 & -1.5 & 0.5 \\
    1 & -2.5 & 2 & -0.5 \\
    -0.5 & 0 & 0.5 & 0 \\
    0 & 1 & 0 & 0
    \end{bmatrix}
    \begin{bmatrix}
    P_{0} \\
    P_{1} \\
    P_{2} \\
    P_{3}
    \end{bmatrix}$

Which, simplified, becomes:

* $P(t) =
    \begin{bmatrix}
    -\frac{1}{2}t^{3} + t^{2} - \frac{1}{2}t & \frac{3}{2}t^{3} -
    \frac{5}{2}t^{2} + 1 & -\frac{3}{2}t^{3} + 2t^{2} + \frac{1}{2}t &
    \frac{1}{2}t^{3} - \frac{1}{2}t^{2}
    \end{bmatrix}
    \begin{bmatrix}
    P_{0} \\
    P_{1} \\
    P_{2} \\
    P_{3}
    \end{bmatrix}$

With further simplification:

* $P(t) = (-\frac{1}{2}t^{3} + t^{2} -\frac{1}{2})P_{0} +
          (\frac{3}{2}t^{3} -\frac{5}{2}t^{2} + 1)P_{1} +
          (-\frac{3}{2}t^{3} + 2t^{2} + \frac{1}{2}t)P_{2} +
          (\frac{1}{2}t^{3} - \frac{1}{2}t^{2})P_{3}$

We also need the derivate at each _t_ in order to correctly orientate the
model.

* $P'(t) =
    \begin{bmatrix}
    3t^{2} & 2t & 1 & 0
    \end{bmatrix}
    \begin{bmatrix}
    -0.5 & 1.5 & -1.5 & 0.5 \\
    1 & -2.5 & 2 & 0.5 \\
    -0.5 & 0 & 0.5 & 0 \\
    0 & 1 & 0 & 0
    \end{bmatrix}
    \begin{bmatrix}
    P_{0} \\
    P_{1} \\
    P_{2} \\
    P_{3}
    \end{bmatrix}$

Which, simplified, becomes:

* $P'(t) =
    \begin{bmatrix}
    -\frac{3}{2}t^{2} + 2t - 0.5 & \frac{9}{2}t^{2} - 5t & -\frac{9}{2}t^{2} +
    4t + 0.5 & \frac{3}{2}t^{2} - t
    \end{bmatrix}
    \begin{bmatrix}
    P_{0} \\
    P_{1} \\
    P_{2} \\
    P_{3}
    \end{bmatrix}$

With further simplification:

* $P'(t) = (-\frac{3}{2}t^{2} + 2t - 0.5)P_{0} +
           (\frac{9}{2}t^{2} - 5t)P_{1} +
           (-\frac{9}{2}t^{2} + 4t + 0.5)P_{2} +
           (\frac{3}{2}t^{2} - t)P_{3}$

Once we know the position to which the translation must occur we must determine
the rotation matrix in order to keep the model correctly oriented.

* $M =
    \begin{bmatrix}
    X_{x} & Y_{x} & Z_{x} & 0 \\
    X_{y} & Y_{y} & Z_{y} & 0 \\
    X_{z} & Y_{z} & Z_{z} & 0 \\
    0 & 0 & 0 & 1
    \end{bmatrix}$

At each _t_ we know that:

* $\vec{X}_{i} = P'(t)$

* $\vec{Z}_{i} = X_{i} \times \vec{Y}_{i - 1}$

* $\vec{Y}_{i} = \vec{Z}_{i} \times \vec{X}_{i}$

