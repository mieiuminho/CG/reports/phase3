## Bezier patches

The point of _Bezier_ patches is to generate a model based on a file that
contains the control points of the _Bezier_ surface. Once we have the surface we
just have to translate to the list of triangles that our _Engine_ application
understands.

There is a formula that allows us to get points of the _Bezier_ surface. The
formula takes two parameters: _u_ and _v_, having that $u \in [0, 1]$ and $v \in
[0, 1]$:


* $p(u, v) = \sum_{i=0}^{n} \sum_{j=0}^{m} B_{i}^{n} (u) B_{j}^{m} (v) k_{i, j}$

In order to evaluate each value of this function for different arguments we need
to establish that a _Bernstein_ polynomial is calculated like this:

* $B_{i}^{n} (u) = \binom ni u^{i} (1 - u)^{n - 1}$

And:

* $\binom ni = \frac{n!}{i!(n - i)!}$

We felt the need to implement these functions in order to get the values that we
needed since we found no suitable library.

Here is the function that calculate the Bernstein polynomial:

```c++
float bernstein(int i, int degree, float u) {

  float c = pow(u, i) * pow(1 - u, degree - 1);
  return combinations(degree, i) * c;

}
```

In which:

```c++
float combinations(int degree, int i) {

  float denominator = float(factorial(i) * factorial(degree - i));
  return (float)(factorial(degree) / denominator);

}
```

The values of _u_ and _v_ really depend on the **tessellation** level that the
user passes as argument to the program. In order to generate the points of
_Bezier_ surface we perform $(tessellation + 1)(tessellation + 1)$ iterations,
and in which iteration the values of _u_ and _v_ are:

* $u = outter_iteration_number / tessellation$
* $v = inner_iteration_number / tessellation$

Using these formulas we generate every point of the _Bezier_ surface, with which
we will create `SimplePlan` instances that will be written to the output file
following the logic that we've already explained.

## Teapot Demo

![Teapot Demo](figures/teapot_patch.png){ width=300px }

