# Introduction {#sec:introduction}

In this phase we are supposed to add the capacity of generating a new type of
model based on _Bezier_ patches. The generator will receive as parameters the
name of a file where the _Bezier_ control points are defined as well as the
required tessellation level. The resulting file will contain a list of the
triangles to draw the surface.

Regarding the engine, we want to extend _translate_ and _rotate_ elements.
Considering the translation, a set of points will be provided to define a
_Catmull-Rom_ cubic curve, as well as the number of seconds to run the whole
curve. The goal is to perform animations based on these curves. The models may
have either a time dependent transform, or a static one as in the previous
phases. In the rotation node, the angle can be replaced with time, meaning the
number of seconds to perform a full 360 degrees rotation around the specified
axis.

In this phase it is also required that models are drawn with **VBOs**, as
opposed to immediate mode used in the previous phases.

The demo scene is a dynamic solar system, including a comet with a trajectory
defined using a _Catmull-Rom_ curve. The comet must be built using Bezier
patches, for instance with the provided control points for the teapot.

\newpage

