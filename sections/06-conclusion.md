# Conclusion {#sec:conclusion}

In this phase we made structural changes to our work: we changed the structure
of our `.3d` files in order to increase the resemblance between the structure
of the file and the memory structure that we build in the engine in order to
draw the models (the vectors of vectors of points and the vector of indexes).
These changes resulted in a way more modular code in the _Generator_ and an
application with a better structure and better code coverage.

We now support more operations in the _Engine_: translation with a provided
time (the time that it takes to complete a full translation) along a
_Catmull-Rom_ curve (with the provided control points) and rotation with a
provided time.

