# Demo {#sec:demo}

![Demo Scene](figures/solar_system.png){ width=600px }

We have two demo scenes: `config.xml` and `config_real.xml`. In the last one we
scaled the rotation speeds according to the reality (based on _Wikipedia_). We
didn't quite maintain the scale in the size of the planets and the Sun because
it would be too big and the moon and some planets would be too small. We
conserved the size relations between the planets (per examples, Saturn is
bigger than the Earth) but picked the sizes in order for the scene to be
somewhat pleasant to watch.

In the `config.xml` we made the Earth's and Saturn's rotation time a little
larger in order for it to be more perceptible.

