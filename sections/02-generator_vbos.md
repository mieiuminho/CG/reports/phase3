# Generator {#sec:generator}

## VBOs

While thinking of a way to implement **VBOs** we noticed that the way in which
we structured the points in our `.3d` files wasn't all that great when it comes
to parsing the models and generating a structure that allows to create the
points and indices vectors (in the _Engine_).

After thinking for a while about a better structure for our `.3d` files (and
since we're free to define the structure of these files) we realized that we
can simply write the points of the model, followed by the indexes (that
represent the order in which those points must be drawn in order to show the
right triangles). The described structure maps directly into the vector of
points and the vector of indexes for each model. To be clear: in the _Engine_ we
simply parse every point and write them in the vector of points (in the order
that they are presented in the `.3d` file). The vector of indexes is generated
in the exact same way.

In order to implement the change that we've just described we felt the need to
make our code a bit more structured: for instance, we now have a structure that
represents the building blocks of each model (`SimplePlan`).

```c++
class SimplePlan {

private:

  vector<Point_3D> points;
  int orientation;

public:

  ...

}
```

This class is meant to represent either a single triangle or two triangles. When
the size of the `points` vector is 3 then `SimplePlan` instance represents 1
triangle. If, on the other hand, it's size is 6 then it represents 2 triangles.
The `orientation` variable allows us to follow the right hand rule in order to
draw the triangles facing the correct direction.

![Single triangle](figures/triangle.png){ width=200px }

![Pair of triangles](figures/quadrado.png){ width=200px }

The `SimplePlan` structure makes it very easy to build a model as a vector of
`SimplePlan` instances. Having this in mind we created the `Model` structure as
follows:

```c++
class Model {

private:

  vector<SimplePlan> plans;
  int no_points;
  int no_indexes;

public:

  ...

}
```

Now, since each `SimplePlan` knows how to write itself to a file (since we store
the orientation) it becomes really simple to write a model to a file: before
writing each point we just write the number of points and then each instance of
`SimplePlan` draws its points. Then we just write the number of indexes and each
instance of `SimplePlan` writes its indexes (in the same order as the points
were written previously).

